Source: overrides
Section: python
Priority: optional
Maintainer: Roland Mas <lolando@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-setuptools,
Standards-Version: 4.6.2.0
Testsuite: autopkgtest-pkg-pybuild
Homepage: https://github.com/mkorpela/overrides
Rules-Requires-Root: no

Package: python3-overrides
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends},
Description: Python Decorator to automatically detect mismatch when overriding a method
 A decorator @override that verifies that a method that should
 override an inherited method actually does it.
 .
 Copies the docstring of the inherited method to the overridden method.
 .
 Since signature validation and docstring inheritance are performed on
 class creation and not on class instantiation, this library
 significantly improves the safety and experience of creating class
 hierarchies in Python without significantly impacting
 performance. See https://stackoverflow.com/q/1167617 for the initial
 inspiration for this library.
 .
 Motivation
 ----------
 .
 Python has no standard mechanism by which to guarantee that (1) a
 method that previously overrode an inherited method continues to do
 so, and (2) a method that previously did not override an inherited
 will not override now.  This opens the door for subtle problems as
 class hierarchies evolve over time. For example,
 .
 1. A method that is added to a superclass is shadowed by an existing
 method with the same name in a subclass.
 .
 2. A method of a superclass that is overridden by a subclass is
 renamed in the superclass but not in the subclass.
 .
 3. A method of a superclass that is overridden by a subclass is
 removed in the superclass but not in the subclass.
 .
 4. A method of a superclass that is overridden by a subclass but the
 signature of the overridden method is incompatible with that of the
 inherited one.
 .
 These can be only checked by explicitly marking method override in
 the code.
 .
 Python also has no standard mechanism by which to inherit docstrings
 in overridden methods. Because most standard linters (e.g., flake8)
 have rules that require all public methods to have a docstring, this
 inevitably leads to a proliferation of ``See parent class for usage``
 docstrings on overridden methods, or, worse, to a disabling of these
 rules altogether. In addition, mediocre or missing docstrings degrade
 the quality of tooltips and completions that can be provided by an
 editor.
